﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_08
{
    class Alumno : Persona
    {
        private int nota;
        public Alumno(string nombre, int edad, char sexo, int nota) : base(nombre, edad, sexo)
        {
            if (nota > 0 && nota <= 10)
            {
                this.nota = nota;
            }
            else
            {
                this.nota = 5;
            }
            darAsisctencia();
        }
        new public void darAsisctencia()
        {
            int numero = base.darAsisctencia();
            if (numero < 50)
            {
                asistencia = true;
            }
            else
            {
                asistencia = false;
            }
        }
        public int Nota
        {
            get
            {
                return nota;
            }
        }
    }
}
