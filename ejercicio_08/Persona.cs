﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_08
{
    class Persona
    {
        protected string nombre;
        protected int edad;
        protected char sexo;
        protected bool asistencia;

        public Persona(string nombre, int edad, char sexo)
        {
            /*if (genero(sexo))
            {
                this.sexo = sexo;
            }
            else
            {
                this.sexo = 'H';
            }*/
            this.nombre = nombre;
            this.edad = edad;
        }
        private bool genero(char a)
        {
            if (a == 'H' || a == 'M')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string Nombre
        {
            get
            {
                return Nombre;
            }
        }
        public bool Asistencia
        {
            get
            {
                return asistencia;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
        }
        public char Sexo
        {
            get
            {
                return sexo;
            }
        }
        protected int darAsisctencia()
        {
            Random rand1 = new Random();
            int genera_asis = rand1.Next(100);
            return genera_asis;
        }
    }
}
