﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_08
{
    class Profesor : Persona
    {
        private string materia;
        public Profesor(string nombre, int edad, char sexo, string materia) : base(nombre, edad, sexo)
        {
            if (materia == "matematicas" || materia == "filosofia" || materia == "fisica")
            {
                this.materia = materia;
            }
            else
            {
                this.materia = "fisica";
            }
            darAsisctencia();
        }
        new private void darAsisctencia()
        {
            int numero = base.darAsisctencia();
            if (numero < 20)
            {
                asistencia = true;
            }
            else
            {
                asistencia = false;
            }
        }
        public string Materia
        {
            get
            {
                return materia;
            }
        }
    }
}
