﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_08
{
    class Aula
    {

        private int id;
        private Profesor profesor;
        private Alumno[] alumnos;
        private string materia;
        private int cantidad_alumnos;

        public Aula(int id, string materia, int cantidad_alumnos)
        {
            this.id = id;
            this.materia = materia;
            this.cantidad_alumnos = cantidad_alumnos;
            alumnos = new Alumno[this.cantidad_alumnos];
            crearAlumnos();
            Console.WriteLine("");
            crearProfesor();
        }
        private void crearAlumnos()
        {
            for (int i = 0; i < cantidad_alumnos; i++)
            {
                Console.WriteLine("Nombre del alumno");
                string nombre = Console.ReadLine();
                Console.WriteLine("Edad");
                int edad = int.Parse(Console.ReadLine());
                Console.WriteLine("H o M para el sexo");
                char sexo = char.Parse(Console.ReadLine());
                Console.WriteLine("Nota del alumno");
                int nota = int.Parse(Console.ReadLine());
                alumnos[i] = new Alumno(nombre, edad, sexo, nota);
            }
        }
        private void crearProfesor()
        {
            Console.WriteLine("Nombre del profesor");
            string nombre = Console.ReadLine();
            Console.WriteLine("Edad del profesor");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Sexo del profesor H o M");
            char sexo = char.Parse(Console.ReadLine());
            Console.WriteLine("Materia del profesor");
            string materia_p = Console.ReadLine();
            profesor = new Profesor(nombre, edad, sexo, materia_p);
        }
        private bool contarAsistencia()
        {
            int cuenta = 0;
            for (int i = 0; i < alumnos.Length; i++)
            {
                if (alumnos[i].Asistencia)
                {
                    cuenta++;
                }
            }
            Console.WriteLine($"Tenemos un total de {cuenta} alumnos presentes");
            if (cuenta > cantidad_alumnos / 2)
            {
                Console.WriteLine("Se puede dar clases");
                return true;
            }
            else
            {
                Console.WriteLine("No se puede dar clases");
                return false;
            }
        }
        public void notas()
        {
            int h_aprobados = 0;
            int m_aprobados = 0;

            for (int i = 0; i < alumnos.Length; i++)
            {
                if (alumnos[i].Nota >= 6)
                {
                    if (alumnos[i].Sexo == 'H')
                    {
                        h_aprobados++;
                    }
                    else
                    {
                        m_aprobados++;
                    }
                }
            }
            Console.WriteLine($"Chicos aprobados son : {h_aprobados} y chicas aprobadas son : {m_aprobados}");
        }
        public bool darClase()
        {
            if (!profesor.Asistencia)
            {
                Console.WriteLine("El profesor no esta no se pueda dar clases");
                return false;
            }
            else if (profesor.Materia != materia)
            {
                Console.WriteLine("El proesor no tiene esta materia");
                return false;
            }
            else if (!contarAsistencia())
            {
                return false;
            }
            else
            {
                Console.WriteLine("Se puede dar clases");
                return true;
            }

        }
    }
}
