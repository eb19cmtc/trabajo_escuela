﻿using System;

namespace ejercicio_08
{
    class Program
    {
        static void Main(string[] args)
        {
            Aula aula1 = new Aula(1, "matematicas", 4);
            if (aula1.darClase())
            {
                aula1.notas();
            }
        }
    }
}
