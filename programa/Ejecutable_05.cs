﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Ejecutable_05
    {
        public void ejec_05()
        {
            Serie[] array_serie = new Serie[5];
            Videojuego[] array_video = new Videojuego[5];

            array_serie[0] = new Serie("grande", 10, "genero", "prueba");
            array_serie[1] = new Serie();
            array_serie[2] = new Serie("medio", 5, "genero5", "prueba_5");
            array_serie[3] = new Serie();
            array_serie[4] = new Serie("pequeño", 2, "genero2", "prueba_2");

            array_video[0] = new Videojuego("Vgrande", 20, "Vgenero", "Vprueba");
            array_video[1] = new Videojuego();
            array_video[2] = new Videojuego("Vmedio", 10, "Vgenero10", "Vprueba_10");
            array_video[3] = new Videojuego();
            array_video[4] = new Videojuego("Vpequeño", 5, "Vgenero5", "Vprueba_5");

            array_serie[0].entregar();
            array_serie[2].entregar();
            array_serie[3].entregar();

            array_video[1].entregar();
            array_video[2].entregar();
            array_video[4].entregar();

            int contar_series = 0;
            int contar_videos = 0;

            for (int i = 0; i < array_serie.Length; i++)
            {
                if (array_serie[i].isEntregado())
                {
                    contar_series++;
                    array_serie[i].devolver();
                }
                if (array_video[i].isEntregado())
                {
                    contar_videos++;
                    array_video[i].devolver();
                }
            }

            Console.WriteLine($"Hay un total de {contar_series} series. Hay un total de {contar_videos} videojuegos. Total {contar_series + contar_videos}");

            Serie serie_compro = array_serie[0];
            Videojuego video_compro = array_video[0];

            for (int j = 1; j < array_serie.Length; j++)
            {
                if (array_serie[j].compareTo(serie_compro) == 1)
                {
                    serie_compro = array_serie[j];
                }
                if (array_video[j].compareTo(video_compro) == 1)
                {
                    video_compro = array_video[j];
                }
            }

            Console.WriteLine("Serie con mas temporadas");
            Console.WriteLine(serie_compro.Titulo);
            Console.WriteLine(serie_compro.Numero_de_temporadas);
            Console.WriteLine(serie_compro.Genero);
            Console.WriteLine(serie_compro.Creador);
            Console.WriteLine("Video con mas horas estimadas");
            Console.WriteLine(video_compro.Titulo);
            Console.WriteLine(video_compro.Horas_estimadas);
            Console.WriteLine(video_compro.Genero);
            Console.WriteLine(video_compro.Compañia);
        }
    }
}
