﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Ejecutable_3
    {
        public void eje3()
        {
            Console.WriteLine("Ingresar el tamaño del array");
            int tamaño = int.Parse(Console.ReadLine());
            Password[] Passwords = new Password[tamaño];
            bool[] password_bool = new bool[tamaño];
            for (int i = 0; i < tamaño; i++)
            {
                Console.WriteLine($"Ingrese la longitud de la password{i + 1}");
                Passwords[i] = new Password(int.Parse(Console.ReadLine()));
                password_bool[i] = Passwords[i].esFuerte();
                if (password_bool[i] == true)
                {
                    Console.WriteLine("Es fuerte");
                }
                else
                {
                    Console.WriteLine("No es fuerte");
                }
                Console.WriteLine("-----------------");
            }
        }
    }
}
