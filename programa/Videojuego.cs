﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Videojuego : IEntregable
    {
        private string titulo;
        private int horas_estimadas;
        private bool entregado;
        private string genero;
        private string compañia;

        public Videojuego()
        {
            titulo = "";
            horas_estimadas = 10;
            entregado = false;
            genero = "";
            compañia = "";
        }
        public Videojuego(string titulo, int horas_estimadas)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
            entregado = false;
            genero = "";
            compañia = "";
        }
        public Videojuego(string titulo, int horas_estimadas, string genero, string compañia)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
            entregado = false;
            this.genero = genero;
            this.compañia = compañia;
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public int Horas_estimadas
        {
            get
            {
                return horas_estimadas;
            }
            set
            {
                horas_estimadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                genero = value;
            }
        }
        public string Compañia
        {
            get
            {
                return compañia;
            }
            set
            {
                compañia = value;
            }
        }
        public void entregar()
        {
            entregado = true;
        }
        public void devolver()
        {
            entregado = false;
        }
        public bool isEntregado()
        {
            if (entregado)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int compareTo(object a)
        {
            Videojuego videojuego_a = (Videojuego)a;
            if (videojuego_a.horas_estimadas < horas_estimadas)
            {
                return 1;
            }
            else if (videojuego_a.horas_estimadas > horas_estimadas)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
    }
}
