﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Password
    {
        private int longitud = 8;
        private string contraseña;

        public Password()
        {
            generarPassword();
        }
        public Password(int logitud)
        {
            this.longitud = logitud;
            generarPassword();
        }
        public string Contraseña
        {
            get
            {
                return contraseña;
            }
        }
        public int Longitud
        {
            get
            {
                return longitud;
            }
            set
            {
                longitud = value;
            }
        }
        public void generarPassword()
        {
            Random rdn = new Random();
            string caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890%$#@";
            int longitud_texto = caracteres.Length;
            char letra;
            string contraseñiaAleatoria = string.Empty;
            for (int i = 0; i < longitud; i++)
            {
                letra = caracteres[rdn.Next(longitud_texto)];
                contraseñiaAleatoria += letra.ToString();
            }
            contraseña = contraseñiaAleatoria;

        }
        public bool esFuerte()
        {
            bool comp = false;
            string mayusculas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int contar_mayu = 0;
            string misnusculas = "abcdefghijklmnopqrstuvwxyz";
            int contar_min = 0;
            string numeros = "1234567890";
            int contar_num = 0;
            int i = 0;
            int j = 0;
            int k = 0;
            while (i < mayusculas.Length)
            {
                if (contraseña.Contains(mayusculas[i]))
                {
                    contar_mayu++;
                }
                else
                {

                }

                i++;
            }
            while (j < misnusculas.Length)
            {
                if (contraseña.Contains(misnusculas[j]))
                {
                    contar_min++;
                }
                else
                {

                }

                j++;
            }
            while (k < numeros.Length)
            {
                if (contraseña.Contains(numeros[k]))
                {
                    contar_num++;
                }
                else
                {

                }

                k++;
            }

            if (contar_mayu > 2)
            {
                if (contar_min > 1)
                {
                    if (contar_num > 5)
                    {
                        comp = true;
                        return comp;
                    }
                }
            }
            return comp;
        }
    }
}
