﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Lavadora : Electrodomestico
    {
        protected const double CARGA_DEFECTO = 5;
        protected double carga;

        public Lavadora()
        {
            carga = CARGA_DEFECTO;
            precio_base = PRECIO_DEFECTO;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
            peso = PESO_DEFECTO;
        }
        public Lavadora(double precio_base, double peso)
        {
            this.precio_base = precio_base;
            this.peso = peso;
            carga = CARGA_DEFECTO;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
        }
        public Lavadora(double precio_base, string color, char consumo_energico, double peso, double carga) : base(precio_base, color, consumo_energico, peso)
        {

            this.carga = carga;
        }
        public double Carga
        {
            get
            {
                return carga;
            }
        }
        new public void precioFinal()
        {
            base.precioFinal();
            int suma_carga = 0;
            if (carga > 30)
            {
                suma_carga += 50;
            }
            precio_base += suma_carga;
        }

    }
}
