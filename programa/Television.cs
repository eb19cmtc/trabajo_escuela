﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Television : Electrodomestico
    {
        protected double resolucion;
        protected bool sinconizadorTDT;

        public Television()
        {
            resolucion = 20;
            sinconizadorTDT = false;
            precio_base = PRECIO_DEFECTO;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
            peso = PESO_DEFECTO;
        }
        public Television(double precio_base, double peso)
        {
            this.precio_base = precio_base;
            this.peso = peso;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
            resolucion = 20;
            sinconizadorTDT = false;
        }
        public Television(double precio_base, string color, char consumo_energico, double peso, double resolucion, bool sinconizadorTDT) : base(precio_base, color, consumo_energico, peso)
        {
            this.resolucion = resolucion;
            this.sinconizadorTDT = sinconizadorTDT;
        }
        public double Resolucion
        {
            get
            {
                return resolucion;
            }
        }
        public bool SincronizadorTDT
        {
            get
            {
                return SincronizadorTDT;
            }
        }
        new public void precioFinal()
        {
            base.precioFinal();
            int suma_s = 0;
            if (resolucion > 40)
            {
                precio_base = precio_base * 1.3;
            }
            if (sinconizadorTDT == true)
            {
                suma_s = 50;
            }
            precio_base += suma_s;
        }
    }
}
