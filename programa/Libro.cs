﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Libro
    {
        private int isbn;
        private string titulo;
        private string autor;
        private int numero_de_paginas;

        public Libro(int isbn, string titulo, string autor, int numero_de_paginas)
        {
            this.isbn = isbn;
            this.titulo = titulo;
            this.autor = autor;
            this.numero_de_paginas = numero_de_paginas;
        }
        public int ISBN
        {
            get
            {
                return isbn;
            }
            set
            {
                isbn = value;
            }
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public string Autor
        {
            get
            {
                return autor;
            }
            set
            {
                autor = value;
            }
        }
        public int Numero_de_paginas
        {
            get
            {
                return numero_de_paginas;
            }
            set
            {
                numero_de_paginas = value;
            }
        }
    }
}
