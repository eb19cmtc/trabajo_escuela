﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Persona
    {
        private string nombre;
        private int edad;
        private int dni;
        private string sexo;
        private double peso;
        private double altura;
        private const string hombre = "H";
        private const int peso_ideal = -1;
        private const int peso_noideal = 0;
        private const int sobrepreso = 1;

        public Persona()
        {
            nombre = "";
            edad = 0;
            sexo = hombre;
            peso = 0;
            altura = 0;
            generarDni();
        }
        public Persona(string nombre, int edad, string sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
            generarDni();
            peso = 0;
            altura = 0;
        }
        public Persona(string nombre, int edad, int dni, string sexo, double peso, double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dni = dni;
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public int Dni
        {
            get
            {
                return dni;
            }
        }
        public string Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                sexo = value;
            }
        }
        public double Peso
        {
            get
            {
                return peso;
            }
            set
            {
                peso = value;
            }
        }
        public double Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value;
            }
        }
        public int calcularIMC()
        {
            double calcular_peso = peso / (altura * 0.01 * 2);
            if (calcular_peso < 20)
            {
                return peso_ideal;
            }
            else if (calcular_peso >= 20 && calcular_peso <= 25)
            {
                return peso_noideal;
            }
            else
            {
                return sobrepreso;
            }
        }
        public bool esMayorDeEdad()
        {
            if (edad >= 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void generarDni()
        {
            Random randm = new Random();
            int numerorandm = randm.Next(10000000, 99999999);
            dni = numerorandm;
        }

    }
}
