﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Serie : IEntregable
    {
        private string titulo;
        private int numero_de_temporadas;
        private bool entregado;
        private string genero;
        private string creador;

        public Serie()
        {
            titulo = "";
            numero_de_temporadas = 3;
            entregado = false;
            genero = "";
            creador = "";
        }
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            numero_de_temporadas = 3;
            entregado = false;
            genero = "";
            this.creador = creador;
        }
        public Serie(string titulo, int numero_de_temporadas, string genero, string creador)
        {
            this.titulo = titulo;
            this.numero_de_temporadas = numero_de_temporadas;
            this.genero = genero;
            this.creador = creador;
            entregado = false;
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public int Numero_de_temporadas
        {
            get
            {
                return numero_de_temporadas;
            }
            set
            {
                numero_de_temporadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                genero = value;
            }
        }
        public string Creador
        {
            get
            {
                return creador;
            }
            set
            {
                creador = value;
            }
        }
        public void entregar()
        {
            entregado = true;
        }
        public void devolver()
        {
            entregado = false;
        }
        public bool isEntregado()
        {
            if (entregado)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int compareTo(object a)
        {
            Serie serie_a = (Serie)a;
            if (serie_a.numero_de_temporadas < numero_de_temporadas)
            {
                return 1;
            }
            else if (serie_a.numero_de_temporadas > numero_de_temporadas)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
    }
}
