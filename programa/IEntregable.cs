﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    public interface IEntregable
    {
        void entregar();
        void devolver();
        bool isEntregado();
        int compareTo(object a);
    }
}
