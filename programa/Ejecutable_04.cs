﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Ejecutable_04
    {
        public void eje_04()
        {
            double suma_elec = 0;
            double suma_lava = 0;
            double suma_tele = 0;
            Electrodomestico[] array_elec = new Electrodomestico[10];
            for (int i = 0; i < 10; i++)
            {
                int numero = 0;
                Console.WriteLine("Escribir 1 para electredomestico, 2 para lavadora , 3 para television");
                numero = int.Parse(Console.ReadLine());
                if (numero == 1)
                {
                    Console.WriteLine("Ingresar el precio base");
                    double precio_base = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingresar el color");
                    string color = Console.ReadLine();
                    Console.WriteLine("Ingrese el consumo energetico");
                    char consumo_energico = char.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese el peso");
                    double peso = double.Parse(Console.ReadLine());

                    array_elec[i] = new Electrodomestico(precio_base, color, consumo_energico, peso);
                    array_elec[i].precioFinal();
                    suma_elec += array_elec[i].Precio_base;
                }
                else if (numero == 2)
                {
                    Console.WriteLine("Ingresar el precio base");
                    double precio_base = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingresar el color");
                    string color = Console.ReadLine();
                    Console.WriteLine("Ingrese el consumo energetico");
                    char consumo_energico = char.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese el peso");
                    double peso = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingresa la carga");
                    double carga = double.Parse(Console.ReadLine());

                    array_elec[i] = new Lavadora(precio_base, color, consumo_energico, peso, carga);
                    array_elec[i].precioFinal();
                    suma_lava += array_elec[i].Precio_base;
                }
                else if (numero == 3)
                {
                    Console.WriteLine("Ingresar el precio base");
                    double precio_base = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingresar el color");
                    string color = Console.ReadLine();
                    Console.WriteLine("Ingrese el consumo energetico");
                    char consumo_energico = char.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese el peso");
                    double peso = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese la resolucion");
                    double resolucion = double.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese 1 si tiene sinconizadorTDT 0 si no");
                    int comprobarsinconizadorTDT = int.Parse(Console.ReadLine());
                    bool sinconizadorTDT = false;
                    if (comprobarsinconizadorTDT == 1)
                    {
                        sinconizadorTDT = true;
                    }
                    else
                    {

                    }

                    array_elec[i] = new Television(precio_base, color, consumo_energico, peso, resolucion, sinconizadorTDT);
                    array_elec[i].precioFinal();
                    suma_tele += array_elec[i].Precio_base;

                }
                else
                {
                    return;
                }
            }
            Console.WriteLine(suma_elec);
            Console.WriteLine(suma_lava);
            Console.WriteLine(suma_tele);
            Console.WriteLine(suma_tele + suma_elec + suma_lava);
        }
    }
}
