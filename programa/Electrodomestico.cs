﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Electrodomestico
    {
        protected const string COLOR_DEFECTO = "blanco";
        protected const char CONSUMO_DEFECTO = 'F';
        protected const double PRECIO_DEFECTO = 100;
        protected const double PESO_DEFECTO = 5;
        protected double precio_base;
        protected string color;
        protected char consumo_energico;
        protected double peso;

        public Electrodomestico()
        {
            precio_base = PRECIO_DEFECTO;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
            peso = PESO_DEFECTO;
        }
        public Electrodomestico(double precio_base, double peso)
        {
            this.precio_base = precio_base;
            this.peso = peso;
            color = COLOR_DEFECTO;
            consumo_energico = CONSUMO_DEFECTO;
        }
        public Electrodomestico(double precio_base, string color, char consumo_energico, double peso)
        {
            this.precio_base = precio_base;
            comprobarColor(color);
            comprobarConsumoEnergico(consumo_energico);
            this.peso = peso;
        }
        public double Precio_base
        {
            get
            {
                return precio_base;
            }
        }
        public string Color
        {
            get
            {
                return color;
            }
        }
        public char Consumo_energico
        {
            get
            {
                return consumo_energico;
            }
        }
        public double Peso
        {
            get
            {
                return peso;
            }
        }
        private void comprobarConsumoEnergico(char letra)
        {
            if (letra == 'A' || letra == 'B' || letra == 'C' || letra == 'D' || letra == 'E' || letra == 'F')
            {
                consumo_energico = letra;
            }
            else
            {
                consumo_energico = CONSUMO_DEFECTO;
            }
        }
        private void comprobarColor(string color)
        {

            string[] colores = new string[5];
            colores[0] = "blanco";
            colores[1] = "negro";
            colores[2] = "rojo";
            colores[3] = "azul";
            colores[4] = "gris";
            bool comprobar = false;
            for (int i = 0; i < 5; i++)
            {
                if (color.Contains(colores[i]) == true)
                {
                    comprobar = true;
                }
            }
            if (comprobar == true)
            {
                this.color = color;
            }
            else
            {
                this.color = COLOR_DEFECTO;
            }
        }
        public void precioFinal()
        {
            int sumar = 0;
            if (consumo_energico == 'A')
            {
                sumar += 100;
            }
            else if (consumo_energico == 'B')
            {
                sumar += 80;
            }
            else if (consumo_energico == 'C')
            {
                sumar += 60;
            }
            else if (consumo_energico == 'D')
            {
                sumar += 50;
            }
            else if (consumo_energico == 'E')
            {
                sumar += 30;
            }
            else
            {
                sumar += 10;
            }

            if (peso > 0 && peso <= 19)
            {
                sumar += 10;
            }
            else if (peso >= 20 && peso <= 49)
            {
                sumar += 50;
            }
            else if (peso >= 50 && peso <= 79)
            {
                sumar += 80;
            }
            else
            {
                sumar += 100;
            }

            precio_base += sumar;

        }

    }
}
