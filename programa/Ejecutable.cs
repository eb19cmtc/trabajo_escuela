﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Ejecutable
    {
        public void eje()
        {
            static void PesoIdeal(Persona p)
            {
                int calpeso = p.calcularIMC();
                if (calpeso == -1)
                {
                    Console.WriteLine("Esta en el peso ideal");
                }
                else if (calpeso == 0)
                {
                    Console.WriteLine("No esta en el peso ideal");
                }
                else
                {
                    Console.WriteLine("Tiene sobrepeso");
                }
            }
            static void MayorEdad(Persona p)
            {
                bool caledad = p.esMayorDeEdad();
                if (caledad == true)
                {
                    Console.WriteLine("Es mayor de edad");
                }
                else
                {
                    Console.WriteLine("No es mayor de edad");
                }
            }

            Console.WriteLine("Escriba el nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Escriba la edad");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresar dni");
            int dni = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese sexo H o M");
            string sexo = Console.ReadLine();
            Console.WriteLine("Escriba el peso");
            double peso = double.Parse(Console.ReadLine());
            Console.WriteLine("Escriba la altura");
            double altura = double.Parse(Console.ReadLine());

            Persona persona1 = new Persona(nombre, edad, dni, sexo, peso, altura);
            Persona persona2 = new Persona(nombre, edad, sexo);
            Persona persona3 = new Persona();

            Console.WriteLine("----------------------------");
            Console.WriteLine("Ingrese peso");
            persona2.Peso = double.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese altura");
            persona2.Altura = double.Parse(Console.ReadLine());

            Console.WriteLine("----------------------------");
            Console.WriteLine("Ingrese Nombre");
            persona3.Nombre = Console.ReadLine();
            Console.WriteLine("Ingrese edad");
            persona3.Edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese sexo H o M");
            persona3.Sexo = Console.ReadLine();
            Console.WriteLine("Ingrese peso");
            persona3.Peso = double.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese altura");
            persona3.Altura = double.Parse(Console.ReadLine());
            Console.WriteLine("----------------------------");



            PesoIdeal(persona1);
            Console.WriteLine("----------------------------");
            PesoIdeal(persona2);
            Console.WriteLine("----------------------------");
            PesoIdeal(persona3);
            Console.WriteLine("----------------------------");

            MayorEdad(persona1);
            Console.WriteLine("----------------------------");
            MayorEdad(persona2);
            Console.WriteLine("----------------------------");
            MayorEdad(persona3);
            Console.WriteLine("----------------------------");

            Console.WriteLine(persona1.Nombre);
            Console.WriteLine(persona1.Edad);
            Console.WriteLine(persona1.Dni);
            Console.WriteLine(persona1.Sexo);
            Console.WriteLine(persona1.Peso);
            Console.WriteLine(persona1.Altura);
            Console.WriteLine("----------------------------");

            Console.WriteLine(persona2.Nombre);
            Console.WriteLine(persona2.Edad);
            Console.WriteLine(persona2.Dni);
            Console.WriteLine(persona2.Sexo);
            Console.WriteLine(persona2.Peso);
            Console.WriteLine(persona2.Altura);
            Console.WriteLine("----------------------------");

            Console.WriteLine(persona3.Nombre);
            Console.WriteLine(persona3.Edad);
            Console.WriteLine(persona3.Dni);
            Console.WriteLine(persona3.Sexo);
            Console.WriteLine(persona3.Peso);
            Console.WriteLine(persona3.Altura);
            Console.WriteLine("----------------------------");


        }
    }
}
