﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Cuenta
    {
        private string titular;
        private double cantidad;

        public Cuenta(string titular)
        {
            this.titular = titular;
        }
        public Cuenta(string titular , double cantidad)
        {
            this.titular = titular;
            this.cantidad = cantidad;
        }
        public string Titular
        {
            get
            {
                return titular;
            }
            set
            {
                titular = value;
            }
        }
        public double Cantidad
        {
            get
            {
                return cantidad;
            }
            set
            {
                cantidad = value;
            }
        }
        public void ingresar(double cantidad)
        {
            if (cantidad > 0)
            {
                this.cantidad += cantidad;
            }
            else
            {
                return;
            }
        }
        public void retirar(double cantidad)
        {


            if (this.cantidad - cantidad < 0)
            {

                this.cantidad = 0;

            }
            else
            {
                this.cantidad -= cantidad;

            }

        }

    }
}
