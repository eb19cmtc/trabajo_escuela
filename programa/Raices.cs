﻿using System;
using System.Collections.Generic;
using System.Text;

namespace programa
{
    class Raices
    {
        private double a;
        private double b;
        private double c;

        public Raices(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public double getDiscriminante()
        {
            return (b * b) - (4 * a * c);
        }
        public void obtenerRaices()
        {
            double x1 = (-b + getDiscriminante()) / 2 * a;
            double x2 = (-b - getDiscriminante()) / 2 * a;
            Console.WriteLine($"X1 : {x1} X2 : {x2}");
        }
        public void obtenerRaiz()
        {
            double x = (-b) / (2 * a);
            Console.WriteLine($"Solucion {x}");
        }
        public bool tieneRaices()
        {
            return getDiscriminante() > 0;
        }
        public bool tieneRaiz()
        {
            return getDiscriminante() == 0;
        }
        public void calcular()
        {
            if (tieneRaices())
            {
                obtenerRaices();
            }
            else if (tieneRaiz())
            {
                obtenerRaiz();
            }
            else
            {
                Console.WriteLine("Sin solucion");
            }
        }
    }
}
