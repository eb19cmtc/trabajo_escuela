﻿using System;

namespace programa
{
    class Program
    {
        static void Main(string[] args)
        {
            /*1ejercicio
            Cuenta cuenta1 = new Cuenta("asd");
            Cuenta cuenta2 = new Cuenta("adf", 100);

            cuenta1.ingresar(100);
            cuenta2.ingresar(400);
            cuenta1.retirar(300);
            cuenta2.retirar(200);

            Console.WriteLine(cuenta1.Cantidad);
            Console.WriteLine(cuenta2.Cantidad);
            */

            /*2ejercicio
            Ejecutable asd = new Ejecutable();
            asd.eje();  
            */
            /*3ejercicio
            Ejecutable_3 nuevo = new Ejecutable_3();
            nuevo.eje3();
             */
            /*4ejercicio
            Ejecutable_04 eje04 = new Ejecutable_04();
            eje04.eje_04();
             */
            /*5ejercicio
            Ejecutable_05 eject = new Ejecutable_05();
            eject.ejec_05(); 
             */
            /*6Ejercicio
           Libro libro1 = new Libro(12345, "Prueba_01", "AP_01", 202);
           Libro libro2 = new Libro(54321, "Prueba_02", "AP_02", 500);

           Console.WriteLine(libro1.Titulo);
           Console.WriteLine(libro1.ISBN);
           Console.WriteLine(libro1.Autor);
           Console.WriteLine(libro1.Numero_de_paginas);

           Console.WriteLine(libro2.Titulo);
           Console.WriteLine(libro2.ISBN);
           Console.WriteLine(libro2.Autor);
           Console.WriteLine(libro2.Numero_de_paginas);

           if (libro1.Numero_de_paginas > libro2.Numero_de_paginas)
           {
               Console.WriteLine($"Libro {libro1.Titulo} Tiene mas paginas, que {libro2.Titulo}");
           }
           else
           {
               Console.WriteLine($"Libro {libro2.Titulo} Tiene mas paginas, que {libro1.Titulo}");
           }
            */
           /*7ejercicio
           Raices raices1 = new Raices(1, -16, 0);
           raices1.calcular();
           */
        }
    }
}
