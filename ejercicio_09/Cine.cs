﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_09
{
    class Cine
    {

        private Asiento[,] asientos;
        private double precio;
        private Pelicula pelicula;

        public Cine(int filas, int columnas, double precio, Pelicula pelicula)
        {
            asientos = new Asiento[filas, columnas];
            this.precio = precio;
            this.pelicula = pelicula;
            rellenarButacas();
        }
        public Asiento[,] Asientos
        {
            get
            {
                return asientos;
            }
        }
        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }
        private void rellenarButacas()
        {
            int fila = asientos.GetLength(0);
            for (int i = 0; i < asientos.GetLength(0); i++)
            {
                for (int j = 0; j < asientos.GetLength(1); j++)
                {
                    asientos[i, j] = new Asiento((char)('A' + j), fila);
                }
                fila--;
            }
        }
        public bool haySitio()
        {
            for (int i = 0; i < asientos.GetLength(0); i++)
            {
                for (int j = 0; j < asientos.GetLength(1); j++)
                {
                    if (!asientos[i, j].ocupado())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool haySitioButaca(int fila, char letra)
        {
            return getAsiento(fila, letra).ocupado();
        }
        public bool sePuedeSentar(Espectador e)
        {
            return e.tieneDinero(precio) && e.tieneEdad(pelicula.EdadMinima);
        }
        public void sentar(int fila, char letra, Espectador e)
        {
            getAsiento(fila, letra).setEspectador(e);
        }
        public Asiento getAsiento(int fila, char letra)
        {
            return asientos[asientos.GetLength(0) - fila - 1, letra - 'A'];
        }
        public int getFilas()
        {
            return asientos.GetLength(0);
        }
        public int getColumnas()
        {
            return asientos.GetLength(1);
        }
        public void monstrar()
        {
            Console.WriteLine("");
            Console.WriteLine($"Pelicula : {pelicula}");
            Console.WriteLine($"Precio : {precio}");
            Console.WriteLine("");
            for (int i = 0; i < asientos.GetLength(0); i++)
            {
                for (int j = 0; j < asientos.GetLength(1); j++)
                {
                    Console.WriteLine($"fila : {i} Letra : {j} Ocupados {asientos[i, j].ocupado()}");

                }
                Console.WriteLine("");
            }
        }
    }
}
