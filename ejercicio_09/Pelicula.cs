﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_09
{
    class Pelicula
    {
        private string titulo;
        private string director;
        private int duracion;
        private int edadMinima;

        public Pelicula(string titulo, string director, int duracion, int edadMinima)
        {
            this.titulo = titulo;
            this.director = director;
            this.duracion = duracion;
            this.edadMinima = edadMinima;
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                titulo = value;
            }
        }
        public string Director
        {
            get
            {
                return director;
            }
            set
            {
                director = value;
            }
        }
        public int Duracion
        {
            get
            {
                return duracion;
            }
            set
            {
                duracion = value;
            }
        }
        public int EdadMinima
        {
            get
            {
                return edadMinima;
            }
            set
            {
                edadMinima = value;
            }
        }
    }
}
