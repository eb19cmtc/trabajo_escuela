﻿using System;

namespace ejercicio_09
{
    class Program
    {
        static void Main(string[] args)
        {
            Pelicula pelicula = new Pelicula("tp", "brayan", 60, 15);

            Console.WriteLine("Ingresar filas");
            int filas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese columnas");
            int columnas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese precio");
            double precio = double.Parse(Console.ReadLine());

            Cine cine = new Cine(filas, columnas, precio, pelicula);

            Console.WriteLine("Numero de espectadores");
            int numEspectadores = int.Parse(Console.ReadLine());

            Espectador e;
            int fila;
            char letra;

            static string generarNombre()
            {
                Random rdn = new Random();
                string[] nombres = new string[5];
                nombres[0] = "nombre_1";
                nombres[1] = "nombre_2";
                nombres[2] = "nombre_3";
                nombres[3] = "nombre_4";
                nombres[4] = "nombre_5";

                int numero = rdn.Next(0, 4);

                return nombres[numero];
            }
            static int generarEdad()
            {
                Random rdn = new Random();
                return rdn.Next(10, 70);
            }
            static double generarDinero()
            {
                Random rdn = new Random();
                return rdn.NextDouble() * (500.00 - 100.00) + 100.00;
            }
            static int numeroAleatorio(int minimo, int maximo)
            {
                Random rdn = new Random();
                return rdn.Next(minimo, maximo);
            }

            for (int i = 0; i < numEspectadores; i++)
            {
                e = new Espectador(generarNombre(), generarEdad(), generarDinero());
                Console.WriteLine($"{e.Nombre}");
                Console.WriteLine($"{e.Edad}");
                Console.WriteLine($"{e.Dinero}");

                do
                {
                    fila = numeroAleatorio(0, cine.getFilas() - 1);
                    letra = (char)numeroAleatorio('A', 'A' + (cine.getColumnas() - 1));
                } while (cine.haySitioButaca(fila, letra));

                if (cine.sePuedeSentar(e))
                {
                    e.pagar(cine.Precio);
                    cine.sentar(fila, letra, e);
                }
            }
            cine.monstrar();
        }
    }
}
