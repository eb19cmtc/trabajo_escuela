﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_09
{
    class Asiento
    {
        private char letra;
        private int fila;
        private Espectador espectador;

        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
            espectador = null;
        }
        public char Letra
        {
            get
            {
                return letra;
            }
            set
            {
                letra = value;
            }
        }
        public int Fila
        {
            get
            {
                return fila;
            }
            set
            {
                fila = value;
            }
        }
        public Espectador Espectador
        {
            get
            {
                return Espectador;
            }

        }
        public bool ocupado()
        {
            return espectador != null;
        }
        public void setEspectador(Espectador espectador)
        {
            this.espectador = espectador;
        }
    }
}
