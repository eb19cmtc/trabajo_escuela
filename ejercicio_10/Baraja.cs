﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicio_10
{
    class Baraja
    {
        public static int NUM_CARTAS = 40;

        private Carta[] cartas;
        private int pSiguienteCarta;

        public Baraja()
        {
            this.cartas = new Carta[NUM_CARTAS];
            this.pSiguienteCarta = 0;
            crearBaraja();
            barajar();
        }
        private static int generarNumeroAleatorio(int minimo, int maximo)
        {
            Random rdn = new Random();
            return rdn.Next(minimo, maximo);
        }
        private void crearBaraja()
        {
            string[] palos = Carta.PALOS;
            for (int i = 0; i < palos.Length; i++)
            {
                for (int j = 0; j < Carta.LIMITE_CARTAS_PALO; j++)
                {
                    if (j != 7 || j != 8)
                    {
                        if (j >= 9)
                        {
                            cartas[((i * (Carta.LIMITE_CARTAS_PALO - 2)) + (j - 2))] = new Carta(j + 1, palos[i]);
                        }
                        else
                        {
                            cartas[((i * (Carta.LIMITE_CARTAS_PALO - 2)) + j)] = new Carta(j + 1, palos[i]);
                        }
                    }
                }
            }
        }
        public void barajar()
        {
            int pAleatoria = 0;
            Carta c;

            for (int i = 0; i < cartas.Length; i++)
            {
                pAleatoria = generarNumeroAleatorio(0, NUM_CARTAS - 1);
                c = cartas[i];
                cartas[i] = cartas[pAleatoria];
                cartas[pAleatoria] = c;
            }
            this.pSiguienteCarta = 0;
        }
        public Carta siguienteCarta()
        {
            Carta c = null;

            if (pSiguienteCarta == NUM_CARTAS)
            {
                Console.WriteLine("Ya no hay cartas");
            }
            else
            {
                c = cartas[pSiguienteCarta++];
            }
            return c;
        }
        public Carta[] cartasDar(int numCartas)
        {
            if (numCartas > NUM_CARTAS)
            {
                Console.WriteLine("No se pueden dar mas cartas");
            }
            else if (cartasDisponible() < numCartas)
            {
                Console.WriteLine("No hay suficiente cartas para monstrar");
            }
            else
            {
                Carta[] cartasDar = new Carta[numCartas];

                for (int i = 0; i < cartasDar.Length; i++)
                {
                    cartasDar[i] = siguienteCarta();
                }
                return cartasDar;
            }
            return null;
        }
        public int cartasDisponible()
        {
            return NUM_CARTAS - pSiguienteCarta;
        }
        public void cartasMonton()
        {
            if (cartasDisponible() == NUM_CARTAS)
            {
                Console.WriteLine("No se saco ninguna carta");
            }
            else
            {
                for (int i = 0; i < pSiguienteCarta; i++)
                {
                    Console.WriteLine($"{cartas[i].Numero} {cartas[i].Palo}");
                }
            }
        }
        public void monstrarBaraja()
        {
            if (cartasDisponible() == 0)
            {
                Console.WriteLine("No hay cartas a monstrar");
            }
            else
            {
                for (int i = 0; i < pSiguienteCarta; i++)
                {
                    Console.WriteLine($"{cartas[i].Numero} {cartas[i].Palo}");
                }
            }
        }
    }
}
